<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', function () {
    return view('index');
});
Route::get('/monitors', function () {
    return view('monitors');
});
Route::get('/accessories', function () {
    return view('accessories');
});
Route::get('/office', function () {
    return view('office');
});
Route::get('/midka', function () {
    return view('midka');
});
Route::get('tasks','TasksController@midka')->name('midka');
Route::get('create','TasksController@create')->name('create');
Route::post('store','TasksController@store')->name('store');

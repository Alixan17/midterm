<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="UTF-8">
	<title>Computers</title>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/style.css">
    <script src="jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="jq.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	</head>
<body id="page-top">
	<div id="flipkart-navbar" class ="sticky">
    <div class="container">
        <div class="row row1">
            <ul class="largenav pull-right">
                <li class="upper-links"><a class="links" href="index">Компьютеры</a></li>
                <li class="upper-links"><a class="links" href="monitors">Мониторы</a></li>
                <li class="upper-links"><a class="links" href="accessories">Аксессуары </a></li>
                <li class="upper-links"><a class="links" href="midka">Midka</a></li>
                </li>
            </ul>
        </div>
        <div class="row row2">
            <div class="col-sm-2">
                <h2 style="margin:0px;"><span class="smallnav menu" onclick="openNav()">☰</span></h2>
                <a class="navbar-brand" href="#"><h3>G<i class="fa fa-adn"></i>L<i class="fa fa-adn">XY</h3></i></a>
            </div>
            <div class="flipkart-navbar-search smallsearch col-sm-8 col-xs-11">
                <div class="row">
                    <input class="flipkart-navbar-input col-xs-11" type="txt" placeholder="Search for Products, Brands and more" name="">
                    <button class="flipkart-navbar-button col-xs-1">
                        <svg width="15px" height="15px">
                            <path d="M11.618 9.897l4.224 4.212c.092.09.1.23.02.312l-1.464 1.46c-.08.08-.222.072-.314-.02L9.868 11.66M6.486 10.9c-2.42 0-4.38-1.955-4.38-4.367 0-2.413 1.96-4.37 4.38-4.37s4.38 1.957 4.38 4.37c0 2.412-1.96 4.368-4.38 4.368m0-10.834C2.904.066 0 2.96 0 6.533 0 10.105 2.904 13 6.486 13s6.487-2.895 6.487-6.467c0-3.572-2.905-6.467-6.487-6.467 "></path>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="cart largenav col-sm-2">
                <a class="cart-button">
                    <svg class="cart-svg " width="16 " height="16 " viewBox="0 0 16 16 ">
                        <path d="M15.32 2.405H4.887C3 2.405 2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644 1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493 1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06 0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0 1.805-.75 1.91-1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86 " fill="#fff "></path>
                    </svg>Корзина
                    <span class="item-number ">0</span>
                </a>
            </div>
        </div>
    </div>
</div>
<div id="mySidenav" class="sidenav">
    <div class="container" style="background-color: #2874f0; padding-top: 10px;">
        <span class="sidenav-heading">Home</span>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
    </div>
    <a href="index">Компьютеры</a>
    <a href="monitors">Мониторы</a>
    <a href="accessories">Аксессуары</a>
    <a href="midka">Midka</a>
</div>
	<br>
	<div id="wrapper">
		<div class="container centered">
			<h1>Компьютеры</h1>
		</div>
	</div><br><br>

	<div class="container">
		<div class="row centered">
			<div class="col-lg-4 thumbnail">
						<a href="#" type="button" data-toggle="modal" data-target=".bs-example-modal-lg">
						<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                         <h5>Общая информация<br></h5>
                         Производительность (быстродействие) ПК – возможность компьютера обрабатывать большие объёмы информации. Определяется быстродействием процессора, объёмом ОП и скоростью доступа к ней (например, Pentium III обрабатывает информацию со скоростью в сотни миллионов операций в секунду)
Производительность (быстродействие) процессора – количество элементарных операций выполняемых за 1 секунду.

Тактовая частота процессора (частота синхронизации) - число тактов процессора в секунду, а такт – промежуток времени (микросекунды) за который выполняется элементарная операция (например сложение). Таким образом Тактовая частота - это число вырабатываемых за секунду импульсов, синхронизирующих работу узлов компьютера. Именно ТЧ определяет быстродействие компьютера

Задается ТЧ специальной микросхемой «генератор тактовой частота», который вырабатывает периодические импульсы. На выполнение процессором каждой операции отводится определенное количество тактов. Частота в 1Мгц = 1миллиону тактов в 1 секунду.  Превышение порога тактовой частоты приводит к возникновению ошибок процессора и др. устройств. Поэтому существуют фиксированные величины тактовых частот для каждого типа процессоров, например: 2,8 ;  3,0  Ггц  и тд

Разрядность процессора – max длина (кол-во разрядов) двоичного кода, который может обрабатываться и передаваться процессором целиком.

Разрядность связана с размером специальных ячеек памяти – регистрами. Регистр в 1байт (8бит) называют восьмиразрядным, в 2байта – 16-разрядным и тд.  Высокопроизводительные компьютеры имеют 8-байтовые регистры (64разряда)

Время доступа - Быстродействие модулей ОП, это период времени, необходимый для считывание min порции информации из ячеек памяти или записи в память. Современные модули обладают скоростью доступа свыше 10нс (1нс=10-9с)

Объем памяти (ёмкость) –  max объем информации, который может храниться в ней.

Плотность записи – объем информации, записанной на единице длины дорожки (бит/мм)

Скорость обмена информации – скорость записи/считывания на носитель, которая определяется скоростью вращения и перемещения этого носителя в устройстве2
                       </div>
                       </div>
                       </div>
							<img src="img/41116-1sv3-Default-WorkingFormat-400Wx400HConversionFormat.jpg" alt="">
							<div class="caption"><p>
								Системный блок Neo Game (Ci7 7700 3,60Ghz/16GB/2000GB/240GB/GT
							</p> <br>
							<p class="price">579<sup>990₸</sup></p>
						</div>
					</a>
					<a class="shop" data-toggle="modal" data-target="#myModal" href=""><i class="fa fa-shopping-cart"></i></a>
     </div>
			<div class="col-lg-4 thumbnail">
					<a href="">
						<a href="office" ><img src="img/31626-11s-Default-WorkingFormat-400Wx400HConversionFormat.jpg" >
						<div class="caption"><p>
							Системный блок Neo Office (Ci5-6400 2,70Ghz/4GB/1000GB/HD510/DVD-
						</p> <br>
						<p class="price">139<sup>990₸</sup></p>
					</div>
				</a>
				<a class="shop" data-toggle="modal" data-target="#myModal"  href="#"><i class="fa fa-shopping-cart"></i></a>
			</div>
			<div class="col-lg-4 thumbnail">
				<a href="">
					<img src="img/41113-1s.jpg" alt="">
					<div class="caption"><p>
						Системный блок Neo Graphics (Ci3-4130 3,40Ghz/4GB/500Gb/GT630 1GB/DVD-RW)
					</p> <br>
					<p class="price">119<sup>990₸</sup></p>
				</div>
				</a>
				<a class="shop" data-toggle="modal" data-target="#myModal"  href=""><i class="fa fa-shopping-cart"></i></a>
			</div>
		</div>
<div class="row">
	<div class="col-lg-4 thumbnail">
		<a href="">
			<img src="img/41659-1s.jpg" alt="">
			<div class="caption"><p>
				Системный блок Neo Multimedia (Ci5-4460 3,20Ghz/4GB/1000Gb/GT640
			</p> <br>
			<p class="price">164<sup>990₸</sup></p>
		</div>
	</a>
	<a class="shop" data-toggle="modal" data-target="#myModal" href=""><i class="fa fa-shopping-cart"></i></a>
</div>
<div class="col-lg-4 thumbnail">
	<a href="">
		<img src="img/41659-1s.jpg" alt="">
		<div class="caption"><p>
			Системный блок Neo Multimedia (Ci7-4790 3,60Ghz/8GB/2000Gb/GT630 2GB/DVD-RW)
		</p> <br>
		<p class="price">224<sup>990₸</sup></p>
	</div>
</a>
<a class="shop" data-toggle="modal" data-target="#myModal" href=""><i class="fa fa-shopping-cart"></i></a>
</div>
<div class="col-lg-4 thumbnail">
	<a href="">
		<img src="img/41866q-1.jpg" alt="">
		<div class="caption"><p>
			Системный блок Neo Game (Ci5-4440 3,10Ghz/8GB/1000Gb/GTX750
		</p> <br>
		<p class="price">199<sup>990₸</sup></p>
	</div>
</a>
<a class="shop" data-toggle="modal" data-target="#myModal" href=""><i class="fa fa-shopping-cart"></i></a>
</div>
</div>
<div class="row">
	<div class="col-lg-4 thumbnail">
		<a href="">
			<img src="img/41866q-1.jpg" alt="">
			<div class="caption"><p>
				Системный блок Neo Game (Ci5-7500 3,40Ghz/16GB/2000Gb/120Gb/GTX
			</p> <br>
			<p class="price">339<sup>990₸</sup></p>
		</div>
	</a>
	<a class="shop" data-toggle="modal" data-target="#myModal" href=""><i class="fa fa-shopping-cart"></i></a>
</div>
<div class="col-lg-4 thumbnail">
	<a href="">
		<img src="img/31626-11s-Default-WorkingFormat-400Wx400HConversionFormat.jpg" alt="">
		<div class="caption"><p>
			Системный блок Neo Game (Ci7-7700 3,60Ghz/8GB/2000Gb/GTX1060
		</p> <br>
		<p class="price">339<sup>990₸</sup></p>
	</div>
</a>
<a class="shop" data-toggle="modal" data-target="#myModal" href=""><i class="fa fa-shopping-cart"></i></a>
</div>
<div class="col-lg-4 thumbnail">
	<a href="">
		<img src="img/41659-1s.jpg" alt="">
		<div class="caption"><p>
			Системный блок Neo Multimedia (Ci7-6700 3,40Ghz/8GB/2000Gb/GT730 2GB/DVDRW)
		</p> <br>
		<p class="price">229<sup>990₸</sup></p>
	</div>
</a>
<a class="shop" data-toggle="modal" data-target="#myModal" href=""><i class="fa fa-shopping-cart"></i></a>
</div>
</div>
<div class="row">
	<div class="col-lg-4 thumbnail">
		<a href="">
			<img src="img/41866q-1.jpg" alt="">
			<div class="caption"><p>
				Системный блок Neo Game (Ci7-6700 3,40Ghz/8GB/1000Gb/GTX1050
			</p> <br>
			<p class="price">249<sup>990₸</sup></p>
		</div>
	</a>
	<a class="shop" data-toggle="modal" data-target="#myModal" href=""><i class="fa fa-shopping-cart"></i></a>
</div>
</div>

</div>
<div class="container">
	<div class="modal fade" id="myModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Заказ</h4>
				</div>
				<div class="modal-body"><input placeholder="Имя" type="text"></div>
				<div class="modal-body"><input placeholder="Номер телефона" type="text"></div>
				<div class="modal-body"><input placeholder="Почта" type="text"></div>
				<div class="modal-footer">
					<button class="btn btn-primary">Заказать</button>
					<button data-dismiss="modal" class="btn btn-default">Закрыть</button></div>
				</div>
			</div>
		</div>
	</div>
  <footer id="myFooter">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 myCols">
                    <h5>Get started</h5>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Sign up</a></li>
                        <li><a href="#">Downloads</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 myCols">
                    <h5>About us</h5>
                    <ul>
                        <li><a href="#">Company Information</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Reviews</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 myCols">
                    <h5>Support</h5>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Help desk</a></li>
                        <li><a href="#">Forums</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 myCols">
                    <h5>Legal</h5>
                    <ul>
                        <li><a href="#">Terms of Service</a></li>
                        <li><a href="#">Terms of Use</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="social-networks">
            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
            <a href="#" class="facebook"><i class="fa fa-facebook-official"></i></a>
            <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
        </div>
        <div class="footer-copyright">
            <p>© 2018 Copyright Text </p>
        </div>
    </footer>
	</body>
</html>
